import {useState, useEffect} from 'react';
import Player from './components/Player/Player';

function App() {
  const [songs] = useState([
    {
      title: "Mohsen yegane",
      artist: "artist 1",
      img_src: "./images/mo.jpg",
      src: "./music/mo.mp3"
    },
    {
      title: "Mohsen chavoshi",
      artist: "Artist 2",
      img_src: "./images/mc.jpg",
      src: "./music/mc.mp3"
    },
    {
      title: "ali brati",
      artist: "Artist 3",
      img_src: "./images/al.jpg",
      src: "./music/bl.mp3"
    },
    {
      title: "Milad gorbani",
      artist: "Artist 4",
      img_src: "./images/ml.jpg",
      src: "./music/ml.mp3"
    }
  ]);

  const [currentSongIndex, setCurrentSongIndex] = useState(0);
  const [nextSongIndex, setNextSongIndex] = useState(0);

  useEffect(() => {
    setNextSongIndex(() => {
      if (currentSongIndex + 1 > songs.length - 1) {
        return 0;
      } else {
        return currentSongIndex + 1;
      }
    });
  }, [currentSongIndex]);

  return (
    <div className="App">
      <Player 
        currentSongIndex={currentSongIndex} 
        setCurrentSongIndex={setCurrentSongIndex} 
        nextSongIndex={nextSongIndex} 
        songs={songs}
      />
    </div>
  );
}

export default App;
